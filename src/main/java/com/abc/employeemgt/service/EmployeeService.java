package com.abc.employeemgt.service;

import com.abc.employeemgt.dto.EmployeeDto;
import com.abc.employeemgt.model.EmployeeDetails;
import org.springframework.stereotype.Service;

import java.util.List;

public interface EmployeeService {
    EmployeeDto addEmployee(EmployeeDto employeeDto);

    List<EmployeeDetails> getAllEmployee();
}
