package com.abc.employeemgt.service.impl;

import com.abc.employeemgt.converter.EmployeeDtoMapper;
import com.abc.employeemgt.dto.EmployeeDto;
import com.abc.employeemgt.model.EmployeeDetails;
import com.abc.employeemgt.repo.EmployeeRepo;
import com.abc.employeemgt.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmployeeServiceImpl implements EmployeeService {

/*    private final EmployeeRepo employeeRepo;

    public EmployeeServiceImpl(EmployeeRepo employeeRepo){
        this.employeeRepo=employeeRepo;
    }*/

    @Autowired
    EmployeeRepo employeeRepo;

    @Autowired
    EmployeeDtoMapper employeeDtoMapper;

    @Override
    public EmployeeDto addEmployee(EmployeeDto employeeDto) {
        EmployeeDetails employeeDetails=employeeDtoMapper.employeeDtoToEmployeeDetails(employeeDto);
        EmployeeDetails responseEmployeeDetails=employeeRepo.save(employeeDetails);
        EmployeeDto employeeDtoSaved=employeeDtoMapper.employeeToEmployeeDto(responseEmployeeDetails);
        return employeeDtoSaved;
    }

    @Override
    public List<EmployeeDetails> getAllEmployee() {
         List<EmployeeDetails> employeeDetailsList=employeeRepo.findAll();

      List<EmployeeDetails> employeeDetails=employeeDetailsList.stream().filter(e ->e.getEmployeeType().equals("Full time")).collect(Collectors.toList());

      return employeeDetails;

    }
}
