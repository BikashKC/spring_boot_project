package com.abc.employeemgt.dto;

import com.abc.employeemgt.model.EmployeeDetails;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeDto {
   Long id;
   String name;
   String employeeType;
}
