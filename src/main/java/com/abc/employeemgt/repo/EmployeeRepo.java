package com.abc.employeemgt.repo;

import com.abc.employeemgt.model.EmployeeDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface EmployeeRepo extends JpaRepository<EmployeeDetails,Long> {
}
