package com.abc.employeemgt.converter;

import com.abc.employeemgt.dto.EmployeeDto;
import com.abc.employeemgt.model.EmployeeDetails;
import org.springframework.stereotype.Component;

@Component
public class EmployeeDtoMapper {
   public EmployeeDto employeeToEmployeeDto(EmployeeDetails employeeDetails){
        EmployeeDto employeeDto=new EmployeeDto();
        employeeDto.setId(employeeDetails.getId());
        employeeDto.setName(employeeDetails.getName());
        employeeDto.setEmployeeType(employeeDetails.getEmployeeType());
        //testPrint();
        return employeeDto;
    }

  public   EmployeeDetails employeeDtoToEmployeeDetails(EmployeeDto employeeDto){
        EmployeeDetails employeeDetails=new EmployeeDetails();
        employeeDetails.setId(employeeDto.getId());
        employeeDetails.setName(employeeDto.getName());
        employeeDetails.setEmployeeType(employeeDto.getEmployeeType());
        return employeeDetails;
    }



/*    void testPrint(){
        System.out.println("This is test call");

    }*/
}
