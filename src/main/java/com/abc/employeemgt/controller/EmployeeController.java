package com.abc.employeemgt.controller;

import com.abc.employeemgt.dto.EmployeeDto;
import com.abc.employeemgt.dto.ExampleDto;
import com.abc.employeemgt.model.EmployeeDetails;
import com.abc.employeemgt.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/employee/v1/")
public class EmployeeController {
    @Autowired
    private EmployeeService employeeService;

    @PostMapping("save-employee")
    public ResponseEntity<?> addEmployeeDetails(@RequestBody EmployeeDto employeeDto){
       EmployeeDto employeeDtoResponse=employeeService.addEmployee(employeeDto);
       return new ResponseEntity<>(employeeDtoResponse,HttpStatus.OK);

    }

    @GetMapping("all")
    public ResponseEntity<?> getAllEmployees(){
        List<EmployeeDetails> employeeDetailsList=employeeService.getAllEmployee();
        return new ResponseEntity<>(employeeDetailsList,HttpStatus.OK);
    }

}

